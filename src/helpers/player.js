import { useState, useEffect } from "react";
import SpotifyPlayer from "react-spotify-web-playback";

/**
 * Render the music player on the landing page
 * @param {*} accessToken access token required for the music player
 * @param trackUri Unique identifier for the track requested
 * @returns Return the music player with the current track and auto play the song
 */
export default function Player({ accessToken, trackUri }) {
  /**
   * Initial state of the play button is set to false
   */
  const [play, setPlay] = useState(false);
  /**
   * Once the player fetches the URI of the song into the player,
   * set the state of the play button to true
   */
  useEffect(() => setPlay(true), [trackUri]);
  /**
   * If no track is returned, return null to the music player landing page
   */
  if (!accessToken) return null;
  return (
    <SpotifyPlayer
      token={accessToken}
      showSaveIcon
      callback={(state) => {
        if (!state.isPlaying) setPlay(false);
      }}
      play={play}
      uris={trackUri ? [trackUri] : []}
    />
  );
}
