# JukeBox
JukeBox is a music streaming application. Once comlpeted this application will have the following features:
1. Discover new music: Search by track name, artist name and album name.
2. Media player: Play, pause and resume the current track, volume controls on the screen
3. Lyrics are displaye don the screen while the song is playing.
4. Like(click on the heart symbol) your favorite songs
5. Handle the controls of the song via a mobile and a web-player

## Built on
1. Node
2. HTML
3. React
4. React-Bootstrap
5. Axios server
6. ESLint

## Getting started
Install all the dependecies by 
npm install

Build your project
npm build

Start the server
npm start

Search for your desired tracks on the web player!

##  Directory structure
./src contains the main files required for this project.
./src/views contains the html views that need to be rendered
./src/resources contains all the images needed as background, logo, etc.
./src/helpers contains helper functions needed for the html views

## Future work
1. Physical playlists can be incorporated
2. Random song generator whcih will fill the landing page with a few(~8) random songs.